package com.adascal.recipesservice.domain.service;

import com.adascal.recipesservice.domain.exception.ErrorRule;
import com.adascal.recipesservice.domain.exception.RecipeServiceException;
import com.adascal.recipesservice.domain.model.Recipe;
import com.adascal.recipesservice.domain.repository.RecipeRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RecipeService {

    private final RecipeRepository recipesRepository;

    public Recipe getRecipe(String userId, String recipeId) {
        var recipe = recipesRepository.findById(recipeId)
                .orElseThrow(() -> new RecipeServiceException(ErrorRule.RECIPE_NOT_FOUND, "Recipe was not found"));

        if (!userId.equals(recipe.getUserId())) {
            throw new RecipeServiceException(ErrorRule.ACCESS_DENIED, "Access denied");
        }
        return recipe;
    }

    public void deleteRecipe(String recipeId, String userId) {
        recipesRepository.deleteByRecipeIdAndUserId(recipeId, userId);
    }

    public Recipe createRecipe(Recipe recipe, String userId) {
        if(recipesRepository.existsByTitleAndUserId(recipe.getTitle(), userId)){
            throw new RecipeServiceException(ErrorRule.RECIPE_ALREADY_EXISTS, "Recipe with the same name already exists");
        }
        recipe.setUserId(userId);
        return recipesRepository.save(recipe);
    }

    public Recipe updateRecipe(String userId, String recipeId, Recipe newRecipe) {
        var recipe = getRecipe(userId, recipeId);
        newRecipe.setId(recipe.getId());
        newRecipe.setUserId(userId);
        return recipesRepository.save(newRecipe);
    }
}
