package com.adascal.recipesservice.infrastructure.mapper;

import com.adascal.recipesservice.domain.model.Recipe;
import com.adascal.recipesservice.infrastructure.dto.RecipeRequest;
import com.adascal.recipesservice.infrastructure.dto.RecipeResponse;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;

@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public interface RecipeMapper {
    RecipeResponse recipeToRecipeResponse(Recipe recipe);

    Recipe recipeToRecipeResponse(RecipeRequest recipe);
}
