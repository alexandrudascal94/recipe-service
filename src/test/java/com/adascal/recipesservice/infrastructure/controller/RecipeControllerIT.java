package com.adascal.recipesservice.infrastructure.controller;

import com.adascal.recipesservice.domain.exception.ErrorRule;
import com.adascal.recipesservice.domain.model.Ingredient;
import com.adascal.recipesservice.domain.model.Recipe;
import com.adascal.recipesservice.domain.repository.RecipeRepository;
import com.adascal.recipesservice.infrastructure.dto.IngredientDto;
import com.adascal.recipesservice.infrastructure.dto.RecipeRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.payconiq.commons.testcontainer.mongodb.annotation.MongoDBTestContainer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Duration;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@MongoDBTestContainer
@AutoConfigureMockMvc
public class RecipeControllerIT {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private RecipeRepository recipeRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void cleanUp() {
        recipeRepository.deleteAll();
    }

    private static final String GET_RECIPE_ENDPOINT = "/v1/recipe/";
    private static final String DELETE_RECIPE_ENDPOINT = "/v1/recipe/";
    private static final String POST_RECIPE_ENDPOINT = "/v1/recipe";
    private static final String PUT_RECIPE_ENDPOINT = "/v1/recipe/";


    @Test
    void get_when_validRecipeAndUserId_should_return200() throws Exception {
        var userId = "1";
        var recipe = createAndSaveTestRecipe(userId);

        mvc.perform(createGetRecipeRequest(recipe.getId(), userId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title").value(recipe.getTitle()));
    }

    @Test
    void get_when_invalidRecipeId_should_return404() throws Exception {
        var randomRecipeId = "2sfes3d";
        mvc.perform(createGetRecipeRequest(randomRecipeId, "3"))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.code").value(ErrorRule.RECIPE_NOT_FOUND.name()));
    }

    @Test
    void get_when_recipeHasDifferentUserId_should_return403() throws Exception {
        var recipe = createAndSaveTestRecipe("2");
        mvc.perform(createGetRecipeRequest(recipe.getId(), "3"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.code").value(ErrorRule.ACCESS_DENIED.name()));
    }

    @Test
    void get_when_InvalidUserId_should_return401() throws Exception {
        var userId = "1";
        var recipe = createAndSaveTestRecipe(userId);
        mvc.perform(createGetRecipeRequest(recipe.getId(), "sdde22222222222222222"))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.code").value(ErrorRule.UNAUTHORIZED.name()));
    }

    @Test
    void delete_when_validRecipeAndUserId_should_return204() throws Exception {
        var userId = "1";
        var recipe = createAndSaveTestRecipe(userId);
        mvc.perform(createDeleteRecipeRequest(recipe.getId(), userId))
                .andExpect(status().isNoContent());

        assertTrue(recipeRepository.findById(recipe.getId()).isEmpty());
    }

    @Test
    void delete_when_invalidUserId_should_return401() throws Exception {
        mvc.perform(createDeleteRecipeRequest("1", "23sf-1?"))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.code").value(ErrorRule.UNAUTHORIZED.name()));
    }

    @Test
    void post_when_validBody_should_return201() throws Exception {
        var userId = "1";
        RecipeRequest recipeRequest = createRecipeRequest();

        mvc.perform(createPostRecipeRequest(userId, objectMapper.writeValueAsString(recipeRequest)))
                .andExpect(status().isCreated());

        assertThat(recipeRepository.findAllByUserId(userId).size()).isEqualTo(1);
    }

    @Test
    void post_when_invalidBody_should_return400() throws Exception {
        var userId = "1";
        RecipeRequest recipeRequest = createRecipeRequest();
        recipeRequest.setTitle("");

        mvc.perform(createPostRecipeRequest(userId, objectMapper.writeValueAsString(recipeRequest)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code").value(ErrorRule.INVALID_INPUT.name()));
        assertThat(recipeRepository.findAllByUserId(userId).size()).isEqualTo(0);
    }

    @Test
    void post_when_invalidUserId_should_return401() throws Exception {
        var userId = "23sd-1";
        RecipeRequest recipeRequest = createRecipeRequest();

        mvc.perform(createPostRecipeRequest(userId, objectMapper.writeValueAsString(recipeRequest)))
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.code").value(ErrorRule.UNAUTHORIZED.name()));
        assertThat(recipeRepository.findAllByUserId(userId).size()).isEqualTo(0);
    }

    @Test
    void post_when_recipeExists_should_return422() throws Exception {
        var userId = "1";
        createAndSaveTestRecipe(userId);
        RecipeRequest recipeRequest = createRecipeRequest();

        mvc.perform(createPostRecipeRequest(userId, objectMapper.writeValueAsString(recipeRequest)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.code").value(ErrorRule.RECIPE_ALREADY_EXISTS.name()));
        assertThat(recipeRepository.findAllByUserId(userId).size()).isEqualTo(1);
    }

    @Test
    void put_when_validRequest_should_return201() throws Exception {
        var userId = "1";
        var recipe = createAndSaveTestRecipe(userId);
        createRecipeRequest();

        assertThat(recipeRepository.findAllByUserId(userId).size()).isEqualTo(1);
        mvc.perform(createUpdateRecipeRequest(userId, recipe.getId(), objectMapper.writeValueAsString(createRecipeRequest())))
                .andExpect(status().isNoContent());
        assertThat(recipeRepository.findAllByUserId(userId).size()).isEqualTo(1);
    }

    @Test
    void put_when_invalidRecipeId_should_return404() throws Exception {
        var userId = "1";
        var recipe = createAndSaveTestRecipe(userId);
        createRecipeRequest();
        mvc.perform(createUpdateRecipeRequest(userId, "32", objectMapper.writeValueAsString(createRecipeRequest())))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.code").value(ErrorRule.RECIPE_NOT_FOUND.name()));

        assertThat(recipeRepository.findAllByUserId(userId).size()).isEqualTo(1);
    }

    @Test
    void put_when_invalidUserId_should_return403() throws Exception {
        var userId = "1";
        var recipe = createAndSaveTestRecipe(userId);
        createRecipeRequest();
        mvc.perform(createUpdateRecipeRequest("32sde", recipe.getId(), objectMapper.writeValueAsString(createRecipeRequest())))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.code").value(ErrorRule.ACCESS_DENIED.name()));

        assertThat(recipeRepository.findAllByUserId(userId).size()).isEqualTo(1);
    }

    private Recipe createAndSaveTestRecipe(String userId) {
        var recipe = Recipe.builder()
                .userId(userId)
                .servings(3)
                .title("carbonara")
                .ingredients(List.of(new Ingredient("Pasta", "kg", 0.3),
                        new Ingredient("eggs", "pieces", 2),
                        new Ingredient("Pepper", "g", 5),
                        new Ingredient("Bacon", "g", 500)))
                .description("Boil the pasta, fry the bacon, mix teh egs with parmesan and all together")
                .isVegetarian(false)
                .preparationTime(Duration.ofMinutes(25))
                .build();

        return recipeRepository.save(recipe);
    }

    private RecipeRequest createRecipeRequest() {
        return RecipeRequest.builder()
                .servings(3)
                .title("carbonara")
                .ingredients(List.of(new IngredientDto("Pasta", "kg", 0.3),
                        new IngredientDto("eggs", "pieces", 2),
                        new IngredientDto("Pepper", "g", 5),
                        new IngredientDto("Bacon", "g", 500)))
                .description("Boil the pasta, fry the bacon, mix teh egs with parmesan and all together")
                .isVegetarian(false)
                .preparationTime(Duration.ofMinutes(25))
                .build();
    }

    private MockHttpServletRequestBuilder createGetRecipeRequest(String recipeId, String userId) {
        String path = GET_RECIPE_ENDPOINT + recipeId;
        return MockMvcRequestBuilders.get(path)
                .servletPath(path)
                .header(HttpHeaders.AUTHORIZATION, userId)
                .contentType(MediaType.APPLICATION_JSON);
    }

    private MockHttpServletRequestBuilder createPostRecipeRequest(String userId, String body) {
        String path = POST_RECIPE_ENDPOINT;
        return MockMvcRequestBuilders.post(path)
                .content(body)
                .servletPath(path)
                .header(HttpHeaders.AUTHORIZATION, userId)
                .contentType(MediaType.APPLICATION_JSON);
    }

    private MockHttpServletRequestBuilder createUpdateRecipeRequest(String userId, String recipeId, String body) {
        String path = PUT_RECIPE_ENDPOINT + recipeId;
        return MockMvcRequestBuilders.put(path)
                .content(body)
                .servletPath(path)
                .header(HttpHeaders.AUTHORIZATION, userId)
                .contentType(MediaType.APPLICATION_JSON);
    }

    private MockHttpServletRequestBuilder createDeleteRecipeRequest(String recipeId, String userId) {
        String path = DELETE_RECIPE_ENDPOINT + recipeId;
        return MockMvcRequestBuilders.delete(path)
                .servletPath(path)
                .header(HttpHeaders.AUTHORIZATION, userId)
                .contentType(MediaType.APPLICATION_JSON);
    }
}
