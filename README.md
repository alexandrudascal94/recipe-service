# recipes-service

This service provides APIs for working with user's recipes. The API spec can be found [here]().

See [this page]() for the architecture decisions.

## Prerequisites

You need [Maven 3.6+](https://maven.apache.org/index.html) configured in your environment.

## Installation

To build and install in your local repository

```maven
mvn clean install
```

## Usage

To run with local configuration

```maven
mvn spring-boot:run -Dspring-boot.run.profiles=local
```

Note: 
 A MongoDB instance should be available at `localhost:27017`.
 The profile must be present because it's used in the database name

## Support

To report or discuss a bug, design change or general questions please contact M&M via [#mm-support](https://payconiq.slack.com/archives/G6KHC6T8T).

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
